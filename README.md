# argocd

#create argocd
kubectl -n argocd -f apply base/install.yaml
kubectl -n argocd apply -f base/argocd-repo-server.yaml
kubectl -n argocd apply -f base/...

#obtain admin password
kubectl -n argocd get secret argocd-initial-admin-secret -o jsonpath="{.data.password}" | base64 -d

#add new cluster
argocd cluster add devops@demo-eks.eu-west-2.eksctl.io --name eks-cluster

#change password
OLD_PWD=$(kubectl get pods -n argocd -l app.kubernetes.io/name=argocd-server -o name | cut -d'/' -f 2)
argocd account update-password --account user1 --current-password "$OLD_PWD" --new-password '123456'

